<?php

	/**
	* 
	*/
	class Database {
		private $hostdb = "localhost";
		private $userdb = "root";
		private $passdb = "";
		private $namedb = "student";
		public $pdo;

		function __construct() {
			if (!isset($this->pdo)) {
				try {
					$link = new PDO("mysql:host=".$this->hostdb.";dbname=".$this->namedb, $this->userdb, $this->passdb);
					$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$link->exec("SET CHARACTER SET utf8");
					$this->pdo = $link;
				} catch (PDOException $e) {
					die("Failed to connect with Database".$e->getMessage());
				}
			}
		}

		public function getStudentData() {
			$sql 	= "SELECT * FROM LIST ORDER BY ID ASC";
			$query 	= $this->pdo->prepare($sql);
			$query->execute();
			$result	= $query->fetchAll();
			return $result;
		}

		

		public function updateStudent($id, $data) {
			/* Student's Information */
			$studentName = $data['studentName'];
			//echo "Student's Name: " . $studentName . "<br />";

			$email = $data['email'];
			//echo "Email: " . $email . "<br />";

			$address = $data['address'];
			//echo "Present Address: " . $address . "<br />";

			$nameOfInstitution = $data['nameOfInstitution'];
			//echo "Name of Institution: " . $nameOfInstitution . "<br />";

			$subject = $data['subject'];
			//echo "Subject: " . $subject . "<br />";

			$passingYear = $data['passingYear'];
			//echo "Passing Year: " . $passingYear . "<br />";

			$dateOfBirth = $data['dateOfBirth'];
			//echo "Date of Birth: " . $dateOfBirth . "<br />";

			$religion = $data['religion'];
			//echo "Religion: " . $religion . "<br />";

			/* Parent's Information */
			$fatherName = $data['fatherName'];
			//echo "Father's Name: " . $fatherName . "<br />";

			$fatherOccupation = $data['fatherOccupation'];
			//echo "Father's Occupation: " . $fatherOccupation . "<br />";

			$fatherContactNo = $data['fatherContactNo'];
			//echo "Father's Contact No: " . $fatherContactNo . "<br />";

			$fatherNIDno = $data['fatherNIDno'];
			//echo "Father's NID No: " . $fatherNIDno . "<br />";

			$motherName = $data['motherName'];
			//echo "Mother's NID No: " . $motherName . "<br />";

			$motherOccupation = $data['motherOccupation'];
			//echo "Mother's Occupation: " . $motherOccupation . "<br />";

			$motherContactNo = $data['motherContactNo'];
			//echo "Mother's Contact No: " . $motherContactNo . "<br />";

			$motherNIDno = $data['motherNIDno'];
			//echo "Mother's NID No: " . $motherNIDno . "<br />";

			//echo "<hr />";

			

			$sql = "UPDATE LIST SET 
						studentName			= :studentName,
						email				= :email,
						address 			= :address,
						nameOfInstitution	= :nameOfInstitution,
						subject				= :subject,
						passingYear			= :passingYear,
						dateOfBirth 		= :dateOfBirth,
						religion			= :religion,

						fatherName			= :fatherName,
						fatherOccupation	= :fatherOccupation,
						fatherContactNo		= :fatherContactNo,
						fatherNIDno 		= :fatherNIDno,

						motherName			= :motherName,
						motherOccupation	= :motherOccupation,
						motherContactNo		=:motherContactNo,
						motherNIDno			= :motherNIDno
					WHERE id				= :id";
			$query = $this->pdo->prepare($sql);
			$query->bindValue(":studentName", $studentName);
			$query->bindValue(":email", $email);
			$query->bindValue(":address", $address);
			$query->bindValue(":nameOfInstitution", $nameOfInstitution);
			$query->bindValue(":subject", $subject);
			$query->bindValue(":passingYear", $passingYear);
			$query->bindValue(":dateOfBirth", $dateOfBirth);
			$query->bindValue(":religion", $religion);
			$query->bindValue(":fatherName", $fatherName);
			$query->bindValue(":fatherOccupation", $fatherOccupation);
			$query->bindValue(":fatherContactNo", $fatherContactNo);
			$query->bindValue(":fatherNIDno", $fatherNIDno);
			$query->bindValue(":motherName", $motherName);
			$query->bindValue(":motherOccupation", $motherOccupation);
			$query->bindValue(":motherContactNo", $motherContactNo);
			$query->bindValue(":motherNIDno", $motherNIDno);
			$query->bindValue(":id", $id);

			$result = $query->execute();

			if ($result) {
				header("Location: index.php");
			} else {
				echo "<script>alert('An error occured!');</script>";
			}

		}

		public function getStudentByID($id) {
			$sql = "SELECT * FROM LIST WHERE id = :id LIMIT 1";
			$query = $this->pdo->prepare($sql);
			$query->bindValue(":id", $id);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function deleteStudentByID($id) {
			$sql = "DELETE FROM LIST WHERE id = :id";
			$query = $this->pdo->prepare($sql);
			$query->bindParam(":id", $id);
			$query->execute();
			header("Location: index.php");
		}






	}



?>