------------------------------------------------------------------------
						Student Information System						
------------------------------------------------------------------------

1.  Project Name:	Student Information System;

2.       Purpose: 	Taking student's information in a automated way;

3.  		Type:	Web Based Application;

4.  	Platform:	Both Online & Offline;

5.  	 Devices:	Desktop, Laptop, Mobile, Tablet, iPad;

6.    Technology:	HTML5, CSS3, JAVASCRIPT, PHP5, PDO;

7. Database Name:	student;

8. 	  Table Name:	list;

9.  Developed By:	S Ahmed Naim.				  DATE: March, 31st 2017
------------------------------------------------------------------------
