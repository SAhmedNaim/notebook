-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2017 at 07:54 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student`
--

-- --------------------------------------------------------

--
-- Table structure for table `list`
--

CREATE TABLE `list` (
  `id` int(10) NOT NULL,
  `studentName` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `nameOfInstitution` varchar(256) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `passingYear` int(10) NOT NULL,
  `dateOfBirth` varchar(256) NOT NULL,
  `religion` varchar(256) NOT NULL,
  `fatherName` varchar(256) NOT NULL,
  `fatherOccupation` varchar(256) NOT NULL,
  `fatherContactNo` varchar(256) NOT NULL,
  `fatherNIDno` varchar(256) NOT NULL,
  `motherName` varchar(256) NOT NULL,
  `motherOccupation` varchar(256) NOT NULL,
  `motherContactNo` varchar(256) NOT NULL,
  `motherNIDno` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list`
--

INSERT INTO `list` (`id`, `studentName`, `email`, `address`, `nameOfInstitution`, `subject`, `passingYear`, `dateOfBirth`, `religion`, `fatherName`, `fatherOccupation`, `fatherContactNo`, `fatherNIDno`, `motherName`, `motherOccupation`, `motherContactNo`, `motherNIDno`) VALUES
(1, 'abc', 'abc@abc.abc', 'abc', 'abc', 'abc', 123, '01-02-03', 'abc', 'abc', 'abc', '123', '123', 'abc', 'abc', '123', '123'),
(2, 'test user', 'test@test.test', 'test', 'test', 'test', 5, '0005-05-05', 'test', 'test', 'test', '5', '5', 'test', 'test', '5', '5'),
(3, 'test', 'test@test.test', 'test', 'test', 'test', 7, '0007-07-07', 'test', 'test', 'test', '7', '7', 'test', 'test', '7', '7'),
(4, 'Naim Ahmed', 'naim.ahmed005@gmail.com', 'Dhanmondi-32.', 'Daffodil International University', 'test', 2, '1995-09-03', 'Islam', 'test', 'test', '7', '7', 'test', 'test', '7', '7'),
(5, 'S Ahmed Naim', 'naim.ahmed035@gmail.com', 'Dhanmondi-32.', 'Daffodil International University', 'B.Sc in Software Engineering', 2, '1995-09-03', 'Islam', 'test', 'test', '7', '7', 'test', 'test', '7', '7'),
(6, 'S Ahmed', 'test@test.test', 'test', 'Daffodil International University', 'B.Sc in Software Engineering', 2, '1995-09-03', 'Islam', 'test', 'test', '7', '7', 'test', 'test', '7', '7'),
(7, 'S Ahmed Naim', 'naim.ahmed035@gmail.com', 'Dhanmondi-32.', 'Daffodil International University', 'B.Sc in Software Engineering', 2019, '1995-09-03', 'Islam', 'test', 'test', '1', '123', 'test', 'test', '1', '456'),
(8, 'S Ahmed Naim', 'naim.ahmed035@gmail.com', 'Dhanmondi-32.', 'Daffodil International University', 'B.Sc in Software Engineering', 2019, '1995-09-03', 'Islam', 'Ahmed Ali Sarker', 'Businessman', '01711443417', '123', 'Mrs. Hasina Ahmed', 'Housewife', '01684993877', '456'),
(9, 'Alamgir Hossain', 'alamgirhossain898@gmail.com', 'Bonani, Dhaka', 'Sheikh Maleka University', 'Masters of Commerce', 2010, '1990-09-16', 'Islam', 'test', 'test', '12345', '12345', 'test', 'test', '12345', '12345'),
(10, 'Naim Ahmed', 'naim.ahmed035@gmail.com', 'Dhanmondi-32.', 'Daffodil International University', 'B.Sc in Software Engineering', 2019, '1995-09-03', 'Islam', 'Ahmed Ali Sarker', 'Businessman', '01711443417', '1234', 'Mrs. Hasina Ahmed', 'Housewife', '01684993877', '5678'),
(15, 'Nurul Huda Sarker', 'nurulhuda@gmail.com', 'Bonani, Dhaka', 'Gaibandha College', 'Masters of Commerce', 2015, '1990-01-01', 'Islam', 'Samsul Haq Sarker', 'test', '12', '34', 'test', 'test', '56', '78');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
