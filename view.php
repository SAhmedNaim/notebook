<?php
    include "header.php";
    include "Database.php";
	$db = new Database();
?>


<?php
	if (isset($_GET['action']) && $_GET['action'] == 'delete') {
		$id = (int)$_GET['id'];
		if ($db->deleteStudentByID($id)) {
			echo "<script>alert('Student deleted successfully!');</script>";
		}
	}
?>



<div class="container-fluid" style="margin-bottom:80px;">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		


		<div class="panel panel-default">
	        <div class="panel-heading">
	        	<h2>
	        		Show All Students
	        	
					<span class="col-lg-4 pull-right">




  <div class="col-lg-6">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Type Student Name..." style="width:250px !important;">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">Search</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->






					</span>
	        	</h2>
	        </div>
			

	        <div class="panel-body">
	        	

				<table class="table table-hover table-striped table-responsive padded_table" style="text-align:center;">
					<tr style="text-align:center; font-weight:bold;">
						<td>Student ID</td>
						<td>Student's Name</td>
						<td>Student's E-mail</td>
						<td>Action</td>
					</tr>








<?php
	$studentData = $db->getStudentData();
	if ($studentData) {
		$i = 0;
		foreach ($studentData as $value) {
			$i++;
			
?>


					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $value["studentName"]; ?></td>
						<td><?php echo $value["email"]; ?></td>
						<td>
							<!-- Action button code start -->
							<!-- All rights reserved by: "http://getbootstrap.com/components/" -->
							<!-- Split button -->
							<div class="btn-group">
							<button type="button" class="btn btn-info">Action</button>
							<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    <span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu">
							    <li>
							    	<a href="insert.php">
							    		<span class="glyphicon glyphicon-plus"> Insert</span>
							    	</a>
							    </li>
							    
							    <li>
							    	<a href="update.php?id=<?php echo $value["id"]; ?>">
							    		<span class="glyphicon glyphicon-pencil"> Update</span>
							    	</a>
							    </li>
							    
							    <li>
							    	<a href="view.php?action=delete&id=<?php echo $value["id"]; ?>" onclick='return confirm("Are you sure you want to delete data?");'>
							    		<span class="glyphicon glyphicon-trash"> Delete</span>
							    	</a>
							    </li>
								
								<!-- separator code start 
							    <li role="separator" class="divider"></li>
							    <li><a href="#">Separated link</a></li>
								 separator code start -->

							</ul>
							</div>
							<!-- Action button code end -->
						</td>
					</tr>


<?php 
		}
	}

?>

				</table>


			<!-- pagination code start -->
			<!-- all rights reserved by: "http://getbootstrap.com/components/#pagination" -->
			<nav aria-label="Page navigation" style="float:right;">
			  <ul class="pagination">
			    <li class="disabled">
			      <a href="#" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <li class="active"><a href="#">1</a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li>
			    <li>
			      <a href="#" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
			<!-- pagination code end -->

		
	        </div>
    	</div>



	</div>
	<div class="col-md-1"></div>

    

</div>

<?php
    include "footer.php";
?>
