<?php
	/* including the header part */
	include "header.php";
	include "Database.php";	
?>









<?php
	
	if (isset($_POST['submit'])) {

		/* Student's Information */
		$studentName = $_POST['studentName'];
		//echo "Student's Name: " . $studentName . "<br />";

		$email = $_POST['email'];
		//echo "Email: " . $email . "<br />";

		$address = $_POST['address'];
		//echo "Present Address: " . $address . "<br />";

		$nameOfInstitution = $_POST['nameOfInstitution'];
		//echo "Name of Institution: " . $nameOfInstitution . "<br />";

		$subject = $_POST['subject'];
		//echo "Subject: " . $subject . "<br />";

		$passingYear = $_POST['passingYear'];
		//echo "Passing Year: " . $passingYear . "<br />";

		$dateOfBirth = $_POST['dateOfBirth'];
		//echo "Date of Birth: " . $dateOfBirth . "<br />";

		$religion = $_POST['religion'];
		//echo "Religion: " . $religion . "<br />";

		/* Parent's Information */
		$fatherName = $_POST['fatherName'];
		//echo "Father's Name: " . $fatherName . "<br />";

		$fatherOccupation = $_POST['fatherOccupation'];
		//echo "Father's Occupation: " . $fatherOccupation . "<br />";

		$fatherContactNo = $_POST['fatherContactNo'];
		//echo "Father's Contact No: " . $fatherContactNo . "<br />";

		$fatherNIDno = $_POST['fatherNIDno'];
		//echo "Father's NID No: " . $fatherNIDno . "<br />";

		$motherName = $_POST['motherName'];
		//echo "Mother's NID No: " . $motherName . "<br />";

		$motherOccupation = $_POST['motherOccupation'];
		//echo "Mother's Occupation: " . $motherOccupation . "<br />";

		$motherContactNo = $_POST['motherContactNo'];
		//echo "Mother's Contact No: " . $motherContactNo . "<br />";

		$motherNIDno = $_POST['motherNIDno'];
		//echo "Mother's NID No: " . $motherNIDno . "<br />";

		//echo "<hr />";

		/* 
		 * Database name: Student,
		 * Table name: List,
		 * Connected by: P.D.O (PHP DATA OBJECTS)
		 */

		/*
		$dsn = "mysql:dbname=student;host=localhost;";
		$user = "root";
		$pass = "";

		try {
			$pdo = new PDO($dsn, $user, $pass);
		} catch (PDOException $e) {
			echo "Connection failed...".$e->getMessage()."<br />";
		}
		*/

		$db = new Database();
	
		$sql = "INSERT INTO LIST(studentName, email, address, nameOfInstitution, subject, passingYear , dateOfBirth, religion, fatherName, fatherOccupation, fatherContactNo, fatherNIDno, motherName, motherOccupation, motherContactNo, motherNIDno) 
			VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $db->pdo -> prepare($sql);
		$arr = array($studentName, $email, $address, $nameOfInstitution, 
			$subject, $passingYear , $dateOfBirth, $religion, 
			$fatherName, $fatherOccupation, $fatherContactNo, $fatherNIDno, 
			$motherName, $motherOccupation, $motherContactNo, $motherNIDno);
		$stmt -> execute($arr) or die("error");

		// redirect to index.php page
		header("Location: index.php");
	}

?>









<div class="col-md-1"></div>
	<div class="col-md-10">
		<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
			<table class="table table-hover table-striped table-responsive padded_table">
					
					<!-- Row Heading -->
					<tr>
						<td class="tableHeader">Student's Information</td>
						<td class="tableHeader">Parent's Information</td>
					</tr>
					
					<!-- 1st Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="name">Student's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="studentName" id="studentName" value="" required />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherName">Father's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="fatherName" id="fatherName" value="" required />
	  						</div>
						</td>
					</tr>
					
					<!-- 2nd Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="email">E-mail ID: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="email" name="email" id="email" value="" required />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherOccupation">
									Father's Occupation: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="fatherOccupation" id="fatherOccupation" value="" required />
	  						</div>
						</td>
					</tr>

					<!-- 3rd Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="address">Present Address: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="address" id="address" value="" required />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherConyactNo">
									Father's Contact No: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="fatherContactNo" id="fatherContactNo" value="" required />
	  						</div>
						</td>
					</tr>
					
					<!-- 4th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="nameOfInstitution">
									Name of Institution: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="nameOfInstitution" 
	  								id="nameOfInstitution" value="" required />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="fatherNIDno">Father's NID No: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="fatherNIDno" id="fatherNIDno" value="" required />
	  						</div>
						</td>
					</tr>
					
					<!-- 5th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="subject">Subject: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="subject" id="subject" value="" required />
	  						</div>
						</td>

						<td class="col-md-5">
							<div class="col-md-5">
								<label for="motherName">Mother's Name: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="motherName" id="motherName" value="" required />
	  						</div>
						</td>
					</tr>
					
					<!-- 6th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="passingYear">Passing Year: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="passingYear" id="passingYear" value="" required />
	  						</div>
						</td>

						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherOccupation">Mother's Occupation: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="motherOccupation" id="motherOccupation" value="" required />
	  						</div>
						</td>
					</tr>
					
					<!-- 7th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="dateOfBirth">Date of Birth: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="date" name="dateOfBirth" id="dateOfBirth" value="" required />
	  						</div>
						</td>
														
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherContactNo">
									Mother's Contact No: 
								</label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="motherContactNo" id="motherContactNo" value="" required />
	  						</div>
						</td>

					</tr>
					
					<!-- 8th Row -->
					<tr>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="religion">Religion: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="text" name="religion" id="religion" value="" required />
	  						</div>
						</td>
						<td class="col-md-6">
							<div class="col-md-5">
								<label for="motherNIDno">Mother's NID No: </label>
	  						</div>
	  						<div class="col-md-1">
	  							<input type="number" name="motherNIDno" id="motherNIDno" value="" required />
	  						</div>
						</td>
					</tr>
					
			</table>
			<div class="submit padded_submit" >
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<input type="submit" name="submit" value="Save" 
						class="btn btn-info btn-submit" />
				</div>
				<div class="col-md-4"></div>
			</div>
		</form>
	</div>
<div class="col-md-1"></div>

<?php
	/* including the footer part */
	include "footer.php";	
?>
